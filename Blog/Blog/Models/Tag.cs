﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Tag
    {
        public int TagID { set; get; }
        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Số lượng kí tự nằm trong khoảng 10-100", MinimumLength = 10)]
        public String Content { set; get; }


        public virtual ICollection<Post> Posts { set; get; }
    }
}