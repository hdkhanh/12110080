﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class Account
    {
        public int AccountID { set; get; }

        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [DataType(DataType.Password)]
        public String Password { set; get; }

        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Địa chỉ emai không đúng")]
        public String Email { set; get; }

        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Tối đa 100 kí tự")]
        public String FirstName { set; get; }

        [Required(ErrorMessage = "Bạn không được bỏ trống")]
        [StringLength(100, ErrorMessage = "Tối đa 100 kí tự")]
        public String LastName { set; get; }



        public virtual ICollection<Post> Posts { set; get; }
    }
}