namespace Blog_5_18_11_2014.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan5 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "DanhThang_ID", "dbo.Danhthangs");
            DropForeignKey("dbo.Danhthangs", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.DanhthangTags", "Danhthang_ID", "dbo.Danhthangs");
            DropForeignKey("dbo.DanhthangTags", "Tag_TagID", "dbo.Tags");
            DropIndex("dbo.Comments", new[] { "DanhThang_ID" });
            DropIndex("dbo.Danhthangs", new[] { "UserProfileUserId" });
            DropIndex("dbo.DanhthangTags", new[] { "Danhthang_ID" });
            DropIndex("dbo.DanhthangTags", new[] { "Tag_TagID" });
            AlterColumn("dbo.Posts", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Posts", "Body", c => c.String(nullable: false));
            AlterColumn("dbo.TinTucs", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.TinTucs", "Body", c => c.String(nullable: false));
            DropColumn("dbo.Comments", "DanhthangdtID");
            DropColumn("dbo.Comments", "DanhThang_ID");
            DropTable("dbo.Danhthangs");
            DropTable("dbo.DanhthangTags");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DanhthangTags",
                c => new
                    {
                        Danhthang_ID = c.Int(nullable: false),
                        Tag_TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Danhthang_ID, t.Tag_TagID });
            
            CreateTable(
                "dbo.Danhthangs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            AddColumn("dbo.Comments", "DanhThang_ID", c => c.Int());
            AddColumn("dbo.Comments", "DanhthangdtID", c => c.Int(nullable: false));
            AlterColumn("dbo.TinTucs", "Body", c => c.String());
            AlterColumn("dbo.TinTucs", "Title", c => c.String());
            AlterColumn("dbo.Posts", "Body", c => c.String());
            AlterColumn("dbo.Posts", "Title", c => c.String());
            CreateIndex("dbo.DanhthangTags", "Tag_TagID");
            CreateIndex("dbo.DanhthangTags", "Danhthang_ID");
            CreateIndex("dbo.Danhthangs", "UserProfileUserId");
            CreateIndex("dbo.Comments", "DanhThang_ID");
            AddForeignKey("dbo.DanhthangTags", "Tag_TagID", "dbo.Tags", "TagID", cascadeDelete: true);
            AddForeignKey("dbo.DanhthangTags", "Danhthang_ID", "dbo.Danhthangs", "ID", cascadeDelete: true);
            AddForeignKey("dbo.Danhthangs", "UserProfileUserId", "dbo.UserProfile", "UserId", cascadeDelete: true);
            AddForeignKey("dbo.Comments", "DanhThang_ID", "dbo.Danhthangs", "ID");
        }
    }
}
