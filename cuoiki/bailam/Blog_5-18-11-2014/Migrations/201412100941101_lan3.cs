namespace Blog_5_18_11_2014.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class lan3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TinTucs",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        Body = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        UserProfileUserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.UserProfile", t => t.UserProfileUserId, cascadeDelete: true)
                .Index(t => t.UserProfileUserId);
            
            CreateTable(
                "dbo.TinTucTags",
                c => new
                    {
                        TinTuc_ID = c.Int(nullable: false),
                        Tag_TagID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.TinTuc_ID, t.Tag_TagID })
                .ForeignKey("dbo.TinTucs", t => t.TinTuc_ID, cascadeDelete: true)
                .ForeignKey("dbo.Tags", t => t.Tag_TagID, cascadeDelete: true)
                .Index(t => t.TinTuc_ID)
                .Index(t => t.Tag_TagID);
            
            AddColumn("dbo.Comments", "TintucttID", c => c.Int(nullable: false));
            AddColumn("dbo.Comments", "Tintuc_ID", c => c.Int());
            AddForeignKey("dbo.Comments", "Tintuc_ID", "dbo.TinTucs", "ID");
            CreateIndex("dbo.Comments", "Tintuc_ID");
        }
        
        public override void Down()
        {
            DropIndex("dbo.TinTucTags", new[] { "Tag_TagID" });
            DropIndex("dbo.TinTucTags", new[] { "TinTuc_ID" });
            DropIndex("dbo.TinTucs", new[] { "UserProfileUserId" });
            DropIndex("dbo.Comments", new[] { "Tintuc_ID" });
            DropForeignKey("dbo.TinTucTags", "Tag_TagID", "dbo.Tags");
            DropForeignKey("dbo.TinTucTags", "TinTuc_ID", "dbo.TinTucs");
            DropForeignKey("dbo.TinTucs", "UserProfileUserId", "dbo.UserProfile");
            DropForeignKey("dbo.Comments", "Tintuc_ID", "dbo.TinTucs");
            DropColumn("dbo.Comments", "Tintuc_ID");
            DropColumn("dbo.Comments", "TintucttID");
            DropTable("dbo.TinTucTags");
            DropTable("dbo.TinTucs");
        }
    }
}
