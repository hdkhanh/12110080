﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_5_18_11_2014.Models
{
    public class Comment
    {
        public int ID { set; get; }

        public String Body { set; get; }
        public DateTime DataCreated { set; get; }

        public int LastTime
        {
            get
            {
                return (DateTime.Now - DataCreated).Minutes;
            }
        }
        public int gio
        {
            get
            {
                return (DateTime.Now - DataCreated).Hours;
            }
        }
        public int ngay
        {
            get
            {
                return (DateTime.Now - DataCreated).Days;
            }
        }

        public int PostID { set; get; }
        public virtual Post Post { set; get; }

  

        public int TintucttID { set; get; }
        public virtual TinTuc Tintuc { set; get; }

    }
}