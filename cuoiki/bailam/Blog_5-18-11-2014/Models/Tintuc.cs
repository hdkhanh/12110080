﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Blog_5_18_11_2014.Models
{
    public class TinTuc
    {
        public int ID { set; get; }
        [Required(ErrorMessage = "không được bỏ trống")]
         public String Title { set; get; }
        [Required(ErrorMessage = "không được bỏ trống")]
        public String Body { set; get; }
        public DateTime DateCreated { set; get; }

        public virtual UserProfile UserProFile { set; get; }
        public int UserProfileUserId { set; get; }

        public virtual ICollection<Comment> Comments { set; get; }
        public virtual ICollection<Tag> Tags { set; get; }
    }
}