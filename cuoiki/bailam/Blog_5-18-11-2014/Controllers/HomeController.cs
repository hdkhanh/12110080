﻿using Blog_5_18_11_2014.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog_5_18_11_2014.Controllers
{
    [Authorize(Users = "admin")]
    public class HomeController : Controller
    
    {
        private BlogDBContext db = new BlogDBContext();

        [AllowAnonymous]

        public ActionResult Index()
        {
            ViewBag.posts = db.Posts.ToList();
            return View(db.Tags.ToList());
        }

       /* public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }*/

       
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
