﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog_5_18_11_2014.Models;

namespace Blog_5_18_11_2014.Controllers
{
    [Authorize(Users = "admin")]
    public class TintucController : Controller
    {
        private BlogDBContext db = new BlogDBContext();

        //
        // GET: /Tintuc/
         [AllowAnonymous]
        public ActionResult Index()
        {
            var tintucs = db.TinTucs.Include(t => t.UserProFile);
            return View(tintucs.ToList());
        }

        //
        // GET: /Tintuc/Details/5
         [AllowAnonymous]
        public ActionResult Details(int id = 0)
        {
            TinTuc tintuc = db.TinTucs.Find(id);
            if (tintuc == null)
            {
                return HttpNotFound();
            }
            ViewData["idpost"] = id;
            return View(tintuc);
        }

        //
        // GET: /Tintuc/Create
        [Authorize(Users = "admin")]
        public ActionResult Create()
        {
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName");
            return View();
        }

        //
        // POST: /Tintuc/Create
        [Authorize(Users = "admin")]
        [HttpPost]
        public ActionResult Create(TinTuc tintuc, string content)
        {
            if (ModelState.IsValid)
            {
                int userID = db.UserProfiles.Select(x => new { x.UserId, x.UserName })
                    .Where(y => y.UserName == User.Identity.Name).Single().UserId;

                tintuc.DateCreated = DateTime.Now;
                // Tao list cac Tag
                List<Tag> Tags = new List<Tag>();
                // Tach cac tag theo dau ,
                string[] TagContent = content.Split(',');
                // Lap cac tag vua tach
                foreach (string item in TagContent)
                {
                    //Tim xem Tag Content da co hay chua
                    Tag TagExits = null;
                    var ListTag = db.Tags.Where(y => y.content.Equals(item));
                    if (ListTag.Count() > 0)
                    {
                        // neu co tag roi thi them post vao 
                        TagExits = ListTag.First();
                        TagExits.TinTucs.Add(tintuc);
                    }
                    else
                    {
                        // neu chua co tag thi tao moi
                        TagExits = new Tag();
                        TagExits.content = item;
                        TagExits.TinTucs = new List<TinTuc>();
                        TagExits.TinTucs.Add(tintuc);
                    }
                    //add vao List cac Tag
                    Tags.Add(TagExits);
                }
                //Gan list Tag cho Post
                tintuc.Tags = Tags;
                tintuc.UserProfileUserId = userID;
                db.TinTucs.Add(tintuc);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", tintuc.UserProfileUserId);
            return View(tintuc);
        }

        //
     
        //
        // GET: /Tintuc/Edit/5
        [Authorize(Users = "admin")]
        public ActionResult Edit(int id = 0)
        {
            TinTuc tintuc = db.TinTucs.Find(id);
            if (tintuc == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", tintuc.UserProfileUserId);
            return View(tintuc);
        }

        //
        // POST: /Tintuc/Edit/5
        [Authorize(Users = "admin")]
        [HttpPost]
        public ActionResult Edit(TinTuc tintuc)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tintuc).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserProfileUserId = new SelectList(db.UserProfiles, "UserId", "UserName", tintuc.UserProfileUserId);
            return View(tintuc);
        }

        //
        // GET: /Tintuc/Delete/5
        [Authorize(Users = "admin")]
        public ActionResult Delete(int id = 0)
        {
            TinTuc tintuc = db.TinTucs.Find(id);
            if (tintuc == null)
            {
                return HttpNotFound();
            }
            return View(tintuc);
        }

        //
        // POST: /Tintuc/Delete/5
        [Authorize(Users = "admin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            TinTuc tintuc = db.TinTucs.Find(id);
            db.TinTucs.Remove(tintuc);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}